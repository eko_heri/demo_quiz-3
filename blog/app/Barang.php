<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    //
    protected $table='barang';
    protected $fillable = ['nama','kondisi','harga','stok','deskripsi','foto','kategori_id','user_id'];
}