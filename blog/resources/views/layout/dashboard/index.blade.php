@extends('layout.master')

@section('content')
<h4>Dashboard</h4>

<link rel="icon" href="{{ asset('template/dist/images/favicon.ico')}}/" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{ asset('/template/dist/pages/waves/css/waves.min.css') }}" type="text/css" media="all">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/dist/css/bootstrap/css/bootstrap.min.css') }}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{ asset('/template/dist/pages/waves/css/waves.min.css') }}" type="text/css" media="all">
    <!-- themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/dist/icon/themify-icons/themify-icons.css') }}">
    <!-- font-awesome-n -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/dist/css/font-awesome-n.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/dist/css/font-awesome.min.css') }}">
    <!-- scrollbar.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/dist/css/jquery.mCustomScrollbar.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/template/dist/asset/css/style.css') }}">

    
<div class="col-xl-12 col-md-12">
    <div class="card mat-stat-card">
        <div class="card-block">
            <div class="row align-items-center b-b-default">
                <div class="col-sm-6 b-r-default p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far fa-user text-c-purple f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>10K</h5>
                            <p class="text-muted m-b-0">Visitors</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="fas fa-volume-down text-c-green f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>100%</h5>
                            <p class="text-muted m-b-0">Volume</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-sm-6 p-b-20 p-t-20 b-r-default">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far fa-file-alt text-c-red f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>2000+</h5>
                            <p class="text-muted m-b-0">Files</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 p-b-20 p-t-20">
                    <div class="row align-items-center text-center">
                        <div class="col-4 p-r-0">
                            <i class="far fa-envelope-open text-c-blue f-24"></i>
                        </div>
                        <div class="col-8 p-l-0">
                            <h5>120</h5>
                            <p class="text-muted m-b-0">Mails</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript" src="{{ asset('template/dist/js/jquery/jquery.min.js') }} "></script>
    <script type="text/javascript" src="{{ asset('template/dist/js/jquery-ui/jquery-ui.min.js') }} "></script>
    <script type="text/javascript" src="{{ asset('template/dist/js/popper.js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/dist/js/bootstrap/js/bootstrap.min.js') }} "></script>
    <!-- waves js -->
    <script src="{{ asset('template/dist/pages/waves/js/waves.min.js') }}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{ asset('template/dist/js/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- slimscroll js -->
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js "></script>

    <!-- menu js -->
    <script src="{{ asset('/template/dist/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('/template/dist/js/vertical/vertical-layout.min.js')}} "></script>
    <script type="text/javascript" src="{{ asset('/template/dist/assets/js/script.js') }} "></script>
@endsection


