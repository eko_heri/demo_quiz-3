@extends('layout.master')

@section('content')
    <h3>Halaman Tambah Produk </h3>
    

    <div class="card">
        <div class="card-header">
            <h5>Form Input Barang</h5>

        </div>
        <div class="card-block">
            <form>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama Produk</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-round" placeholder="Type your title in Placeholder">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Barang</label>
                    <div class="col-sm-10">
                        <select name="select" class="form-control">
                            <option value="opt1">Baru</option>
                            <option value="opt2">Bekas</option>
                        </select>
                    </div>
                </div>
       
                  
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pilih Kategori</label>
                    <div class="col-sm-10">
                        <select name="select" class="form-control">
                            <option value="opt1">Buku</option>
                            <option value="opt2">Dapur</option>
                            <option value="opt3">Elektronik</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Harga</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-round" placeholder="masukkan harga barang" maxlength="10">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Stok</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-round" placeholder="masukkan stok" maxlength="2">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Upload Product</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control form-control-round">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Deskripsi Produk</label>
                    <div class="col-sm-10">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Detail Produk"></textarea>
                    </div>
                </div>
            </form>
            <button class="btn btn-success waves-effect waves-light">Tambah Produk</button>
            <button class="btn waves-effect waves-light btn-danger"><i class="icofont icofont-info-square"></i>Reset</button>
@endsection