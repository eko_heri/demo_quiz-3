@extends('layout.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Tambah Kategori</h5>

        </div>
        <div class="card-block">
            <form>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama Kategori</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-round" placeholder="Type your title in Placeholder">
                    </div>
                </div>
              
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Deskripsi Produk</label>
                    <div class="col-sm-10">
                        <textarea rows="5" cols="5" class="form-control" placeholder="Detail Produk"></textarea>
                    </div>
                </div>
            </form>
            <button class="btn btn-success waves-effect waves-light">Tambah Kategori</button>
@endsection