<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <nav class="pcoded-navbar">
            <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
            <div class="pcoded-inner-navbar main-menu">
                <div class="">
                    <div class="main-menu-header">
                        <img class="img-80 img-radius" src="{{ asset('template/dist/images/avatar-4.jpg') }}" alt="User-Profile-Image">
                        <div class="user-details">
                            <span id="more-details">John Doe<i class="fa fa-caret-down"></i></span>
                        </div>
                    </div>
                    <div class="main-menu-content">
                        <ul>
                            <li class="more-details">

                                <a href="#!"><i class="ti-settings"></i>Settings</a>
                                <a href="#"><i class="ti-layout-sidebar-left"></i>Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="pcoded-navigation-label">Navigation</div>
                <ul class="pcoded-item pcoded-left-item">
                    <li class="pcoded-hasmenu">
                        <a href="/post" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-home"></i><b></b></span>
                            <span class="pcoded-mtext">Dashboard</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>


                <div class="pcoded-navigation-label">Profil</div>
                <ul class="pcoded-item pcoded-left-item">
                    <li class="">
                        <a href="biodata" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-layers"></i><b>FC</b></span>
                            <span class="pcoded-mtext">Biodata</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
                <div class="pcoded-navigation-label">Daftar Produk</div>
                <ul class="pcoded-item pcoded-left-item">
                    <li class="">
                        <a href="/post/create" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-receipt"></i><b>B</b></span>
                            <span class="pcoded-mtext">Tambah Barang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="/post/show" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-receipt"></i><b>B</b></span>
                            <span class="pcoded-mtext">Daftar Barang</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
                <div class="pcoded-navigation-label">Kategori</div>
                <ul class="pcoded-item pcoded-left-item">
                    <li class="">
                        <a href="/kategori" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-bar-chart-alt"></i><b>C</b></span>
                            <span class="pcoded-mtext">Tambah Kategori</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
